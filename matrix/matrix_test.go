package matrix

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestFrom(t *testing.T) {
	// when
	m := From(1, 2, 3, 4, 5)

	// then
	exp := &Matrix{width: 1, height: 5, data: []float64{1, 2, 3, 4, 5}}
	assert.Equal(t, exp, m)
}

func TestMatrix_Get(t *testing.T) {
	// given
	m := From(1, 2, 3, 4, 5)

	// then
	assert.Equal(t, 1.0, m.Get(0, 0))
	assert.Equal(t, 5.0, m.Get(0, 4))
}

func TestMatrix_Add(t *testing.T) {
	// given
	m1 := NewData(3, 3, []float64{1, 2, 3, 4, 5, 6, 7, 8, 9})
	m2 := NewData(3, 3, []float64{9, 8, 7, 6, 5, 4, 3, 2, 1})

	// when
	r := m1.AddM(m2)

	// then
	exp := NewData(3, 3, []float64{10, 10, 10, 10, 10, 10, 10, 10, 10})
	assert.Equal(t, exp, r)
}

func TestMatrix_Sub(t *testing.T) {
	// given
	m1 := NewData(3, 3, []float64{10, 10, 10, 10, 10, 10, 10, 10, 10})
	m2 := NewData(3, 3, []float64{1, 2, 3, 4, 5, 6, 7, 8, 9})

	// when
	r := m1.SubM(m2)

	// then
	exp := NewData(3, 3, []float64{9, 8, 7, 6, 5, 4, 3, 2, 1})
	assert.Equal(t, exp, r)
}

func TestMatrix_Transpose(t *testing.T) {
	// given:
	//   2x3 matrix
	//   1 2
	//   3 4
	//   5 6
	m := NewData(2, 3, []float64{1, 2, 3, 4, 5, 6})

	// when
	r := m.Transpose()

	// then:
	//   expect 3x2 matrix
	//   1 3 5
	//   2 4 6
	exp := NewData(3, 2, []float64{1, 3, 5, 2, 4, 6})
	assert.Equal(t, exp, r)
}

func TestMatrix_Mul(t *testing.T) {
	// given:
	//   3x2 matrix
	//   1 2 3
	//   4 5 6
	m1 := NewData(3, 2, []float64{1, 2, 3, 4, 5, 6})

	// 2x3 matrix
	//  7  8
	//  9 10
	// 11 12
	m2 := NewData(2, 3, []float64{7, 8, 9, 10, 11, 12})

	// when
	r := m1.MulM(m2)

	// then
	//   expect 2x2 matrix
	//    58  64
	//   139 154
	exp := NewData(2, 2, []float64{58, 64, 139, 154})
	assert.Equal(t, exp, r)
}
