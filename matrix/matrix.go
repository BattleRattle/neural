package matrix

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"math/rand"
)

type Matrix struct {
	width  int
	height int
	data   []float64
}

func New(width int, height int) *Matrix {
	return NewData(width, height, make([]float64, width*height))
}

func NewData(width int, height int, data []float64) *Matrix {
	if width*height != len(data) {
		panic("Invalid width / height for provided data")
	}

	m := new(Matrix)
	m.width = width
	m.height = height
	m.data = data

	return m
}

func From(data ...float64) *Matrix {
	return NewData(1, len(data), data).Copy()
}

func (m *Matrix) Add(n float64) *Matrix {
	return m.Map(func(e float64, x int, y int) float64 {
		return e + n
	})
}

func (m *Matrix) AddM(o *Matrix) *Matrix {
	if m.width != o.width || m.height != m.height {
		panic("Width / height mismatch")
	}

	return m.Map(func(e float64, x int, y int) float64 {
		return e + o.Get(x, y)
	})
}

func (m *Matrix) SubM(o *Matrix) *Matrix {
	if m.width != o.width || m.height != m.height {
		panic("Width / height mismatch")
	}

	return m.Map(func(e float64, x int, y int) float64 {
		return e - o.Get(x, y)
	})
}

func (m *Matrix) Mul(n float64) *Matrix {
	return m.Map(func(e float64, x int, y int) float64 {
		return e * n
	})
}

func (m *Matrix) MulM(o *Matrix) *Matrix {
	if m.width != o.height {
		panic(fmt.Sprintf("Wrong width / height for matrix multiplication (m = %dx%d, o = %dx%d)", m.width, m.height, o.width, o.height))
	}

	return New(o.width, m.height).Map(func(e float64, x int, y int) float64 {
		sum := 0.0

		for i := 0; i < m.width; i++ {
			sum += m.Get(i, y) * o.Get(x, i)
		}

		return sum
	})
}

func (m *Matrix) Hadamard(o *Matrix) *Matrix {
	if m.width != o.width || m.height != o.height {
		panic(fmt.Sprintf("Wrong width / height for hadamard product (m = %dx%d, o = %dx%d)", m.width, m.height, o.width, o.height))
	}

	return m.Map(func(e float64, x int, y int) float64 {
		return e * o.Get(x, y)
	})
}

func (m *Matrix) Copy() *Matrix {
	cp := new(Matrix)
	cp.width = m.width
	cp.height = m.height
	cp.data = make([]float64, len(m.data))

	for i := range m.data {
		cp.data[i] = m.data[i]
	}

	return cp
}

func (m *Matrix) Get(x int, y int) float64 {
	if x < 0 || x >= m.width || y < 0 || y >= m.height {
		panic("Invalid offset")
	}

	return m.data[x+y*m.width]
}

func (m *Matrix) Debug() *Matrix {
	logrus.Infof("%+v", m)

	return m
}

func (m *Matrix) Transpose() *Matrix {
	return New(m.height, m.width).Map(func(e float64, x int, y int) float64 {
		return m.Get(y, x)
	})
}

func (m *Matrix) Randomize() *Matrix {
	return New(m.width, m.height).Map(func(e float64, x int, y int) float64 {
		return rand.Float64()
	})
}

func (m *Matrix) Flatten() []float64 {
	return m.data
}

func (m *Matrix) Map(fn MapFunc) *Matrix {
	cp := new(Matrix)
	cp.width = m.width
	cp.height = m.height
	cp.data = make([]float64, len(m.data))

	for i := range m.data {
		cp.data[i] = fn(m.data[i], i%m.width, i/m.width)
	}

	return cp
}

type MapFunc func(e float64, x int, y int) float64

func SimpleMapFunc(fn func(e float64) float64) MapFunc {
	return func(e float64, x int, y int) float64 {
		return fn(e)
	}
}
