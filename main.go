package main

import (
	"bitbucket.org/BattleRattle/neural/network"
	"github.com/sirupsen/logrus"
	"math/rand"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	for op, pairs := range map[string][]testPair{"OR": or, "AND": and, "XOR": xor} {
		logrus.Infof("========== %s ==========", op)

		nw := network.New(2, 4, 1)

		logrus.Info("Before Training:")
		predict(nw, op)

		for i := 0; i < 100000; i++ {
			train(nw, pairs)
		}

		logrus.Info("After Training:")
		predict(nw, op)
	}
}

func train(nw *network.Network, pairs []testPair) {
	for _, v := range rand.Perm(len(pairs)) {
		nw.Train(pairs[v].input, pairs[v].output)
	}
}

func predict(nw *network.Network, op string) {
	logrus.Infof("  0 %s 0 = %.2f", op, nw.Predict([]float64{0, 0})[0])
	logrus.Infof("  0 %s 1 = %.2f", op, nw.Predict([]float64{0, 1})[0])
	logrus.Infof("  1 %s 0 = %.2f", op, nw.Predict([]float64{1, 0})[0])
	logrus.Infof("  1 %s 1 = %.2f", op, nw.Predict([]float64{1, 1})[0])
}

type testPair struct {
	input  []float64
	output []float64
}

var xor = []testPair{
	{input: []float64{0, 0}, output: []float64{0}},
	{input: []float64{0, 1}, output: []float64{1}},
	{input: []float64{1, 0}, output: []float64{1}},
	{input: []float64{1, 1}, output: []float64{0}},
}

var or = []testPair{
	{input: []float64{0, 0}, output: []float64{0}},
	{input: []float64{0, 1}, output: []float64{1}},
	{input: []float64{1, 0}, output: []float64{1}},
	{input: []float64{1, 1}, output: []float64{1}},
}

var and = []testPair{
	{input: []float64{0, 0}, output: []float64{0}},
	{input: []float64{0, 1}, output: []float64{0}},
	{input: []float64{1, 0}, output: []float64{0}},
	{input: []float64{1, 1}, output: []float64{1}},
}
