package network

import (
	"bitbucket.org/BattleRattle/neural/activation"
	"bitbucket.org/BattleRattle/neural/matrix"
	"fmt"
)

type Network struct {
	layers []int

	weights []*matrix.Matrix
	biases  []*matrix.Matrix

	learningRate float64
	activation   activation.Func
}

// New creates a new neural network using dense layers in the specified amounts of neurons per layer.
// First layer is the input layer, last one is the output layer. Everything in between are hidden layers.
func New(layers ...int) *Network {
	if len(layers) < 2 {
		panic("Too few layers, at least input and output must be provided")
	}

	n := new(Network)

	n.layers = layers

	n.weights = make([]*matrix.Matrix, len(layers)-1)
	n.biases = make([]*matrix.Matrix, len(layers)-1)

	for i := 0; i < len(layers)-1; i++ {
		n.weights[i] = matrix.New(layers[i], layers[i+1]).Randomize()
		n.biases[i] = matrix.New(1, layers[i+1]).Randomize()
	}

	n.activation = activation.Sigmoid
	n.learningRate = 0.1

	return n
}

// Predict calculates the output for the given input.
func (n *Network) Predict(inputs []float64) []float64 {
	if len(inputs) != n.layers[0] {
		panic(fmt.Sprintf("Provided %d input values, but input layer requires %d values", len(inputs), n.layers[0]))
	}

	layer := matrix.From(inputs...)

	for i := 0; i < len(n.layers)-1; i++ {
		layer = n.weights[i].MulM(layer).AddM(n.biases[i]).Map(matrix.SimpleMapFunc(n.activation.Func))
	}

	return layer.Flatten()
}

// Train uses input and output values to train the network. Errors are back-propagated in order to adjust weights and biases.
func (n *Network) Train(inputs []float64, outputs []float64) {
	// predict output
	m := make([]*matrix.Matrix, len(n.layers))
	m[0] = matrix.From(inputs...)
	for i := 0; i < len(n.layers)-1; i++ {
		m[i+1] = n.weights[i].MulM(m[i]).AddM(n.biases[i]).Map(matrix.SimpleMapFunc(n.activation.Func))
	}

	// calculate and propagate errors
	err := matrix.From(outputs...).SubM(m[len(m)-1])
	for i := len(m) - 2; i >= 0; i-- {
		gradients := m[i+1].Map(matrix.SimpleMapFunc(n.activation.DFunc)).Hadamard(err).Mul(n.learningRate)
		delta := gradients.MulM(m[i].Transpose())
		n.weights[i] = n.weights[i].AddM(delta)
		n.biases[i] = n.biases[i].AddM(gradients)
		err = n.weights[i].Transpose().MulM(err)
	}
}
