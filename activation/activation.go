package activation

import "math"

type Func struct {
	Func  func(x float64) float64
	DFunc func(y float64) float64
}

var Sigmoid = Func{
	Func: func(x float64) float64 {
		return 1.0 / (1.0 + math.Exp(-x))
	},
	DFunc: func(y float64) float64 {
		return y * (1 - y)
	},
}
