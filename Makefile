GO_PKG := bitbucket.org/BattleRattle/neural
BINARY := dist/neural

.PHONY: all dep dep-update test test-verbose bench cs

all: test

$(BINARY): $(shell find . -type f -name '*.go')
	@echo "[compile] $@"
	go build -o $@ .

run: $(BINARY)
	@echo "[run] $@"
	$(BINARY)

dep:
	@echo "[ensure deps]"
	dep ensure -v

dep-update:
	@echo "[update deps]"
	dep ensure -update -v

test:
	go test $(GO_PKG)/...

test-verbose:
	go test -v -cover $(GO_PKG)/...

bench:
	go test -bench=. $(GO_PKG)/...

cs:
	@echo "[code style]"
	go fmt ./...
	golint $(shell go list ./...)
	go vet ./...
